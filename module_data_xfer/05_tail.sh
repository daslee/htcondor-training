#!/bin/bash

for i in {1..500} ; do
	echo "output iteration ${i}" >> output.txt
	echo "scratch iteration ${i}" >> scratch.txt
	sleep 5
done
