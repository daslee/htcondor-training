#!/bin/bash

if (( RANDOM % 2)) ; then 
    echo "Hello from $1"
    exit 0  
else 
    exit 1 
fi
