#!/bin/sh

echo 'Date:    ' $(date)
echo 'Host:    ' $(hostname)
echo 'System:  ' $(uname -spo)

echo 'Home:    ' $HOME
echo 'Workdir: ' $PWD
echo 'Path:   ' $PATH

echo "Program: $0"
echo "Args:    $*"

